'use strict';
(function(angular, jQuery) {
    angular.module('manyAmigosClient', ['ngSanitize'])
        .config(['$interpolateProvider', function($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        }])
        .run(function() {
            jQuery(document).foundation();
            new WOW().init();
        });
})(angular, jQuery);