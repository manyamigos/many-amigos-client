angular.module('manyAmigosClient').directive('maVideo', ['$sce', '$window', '$timeout', function($sce, $window, $timeout) {
    return {
        restrict: 'A',
        templateUrl: 'templates/directives/ma-video.tpl.html',
        scope: {
            url: '@'
        },
        link: function($scope, $element) {

            /**
             * On initialization we can take either a youtube url (http://youtube.com/watch?v=hsdgfgjh)
             * or an embed link (//youtube.com/embed/sdkfjh) and retrieve only the code out of
             * the string, once we have the string, we can build the actual video URL
             */
            $scope.videoUrl = null;
            (function(initialUrl) {
                var tempUrl      = angular.copy(initialUrl),
                    embedPattern = '/embed/',
                    embedIndex   = tempUrl.indexOf(embedPattern),
                    qsIndex      = tempUrl.indexOf('?'),
                    videoCode    = null,
                    startIndex   = 0,
                    endIndex     = 0;

                // If it is, then retrieve the code from the url
                if (embedIndex !== -1) {
                    startIndex = embedIndex + embedPattern.length;
                    endIndex   = (qsIndex !== -1) ? (tempUrl.length - startIndex) : (tempUrl.length - qsIndex);
                    videoCode  = tempUrl.substring(startIndex, endIndex);
                } else {
                    // Look for the v= tag
                    var codePattern = 'v=';
                    if (tempUrl.indexOf(codePattern) !== -1) {
                        var urlSegmentsArray = tempUrl.split('&');
                        urlSegmentsArray.some(function(segment) {
                            var codeIndex = segment.indexOf(codePattern);
                            if (codeIndex !== -1) {
                                startIndex = codeIndex + codePattern.length;
                                endIndex   = segment.length;
                                videoCode  = segment.substring(startIndex, endIndex);
                            }
                            return (codeIndex !== 1);
                        });
                    }
                }

                $scope.videoUrl    = $sce.trustAsResourceUrl('//youtube.com/embed/' + videoCode);
                $scope.originalUrl = angular.copy($scope.videoUrl);

            })($scope.url);

            /**
             * This is the element resizing logic
             */
            $scope.setEmbedSize  = function() {
                $scope.width  = $element.width();
                $scope.height = $element.height();
                console.log($scope.width, $scope.height);
            };
            $scope.setEmbedSize();
            var timeout;
            angular.element($window).bind('resize', function() {
                $timeout.cancel(timeout);
                timeout = $timeout(function() {
                    $scope.setEmbedSize();
                    $scope.$apply();
                }, 100);
            });
        },
        controller: ['$scope', '$sce', function($scope, $sce) {
            $scope.videoPlaying = false;
            $scope.startVideo = function() {
                if ($scope.videoUrl !== null) {
                    $scope.videoPlaying = true;
                }
            }
        }]
    };
}]);