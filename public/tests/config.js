(function(global) {
    global.monkeytestjs = {
        "facebookId": "000000000000000",

        "local": {
            "env": [/manyamigos.dev/]
        },
        "stage": {
            "env": [/stage.manyamigos.com/],
            "facebookId": "222222222222222"
        },
        "beta": {
            "env": [/BETA URL OR PART OF/],
            "facebookId": "33333333333333333"
        },
        "production": {
            "env": [/manyamigos.com/],
            "facebookId": "4444444444444444444"
        },
        "testsDir": "mytests",
        "globalTests": [
            "global/not_server_error.js",
            "global/is_html_w3c_valid.js",
            "global/has_utf8_metatag.js",
            "global/has_google_analytics.js"
        ],
        "pages": [
            { url: "/" },
            { url: "/contact-us" },
            { url: "/faq" },
            { url: "/how-it-works" },
            { url: "/how-it-works/investing" },
            { url: "/how-it-works/raising-money" },
            { url: "/members" },
            { url: "/members/123" },
            { url: "/privacy-policy" },
            { url: "/terms-of-use" }
        ],
        "proxyUrl": "core/proxy.php?mode=native&url=<%= url %>",
        "loadSources": true
    };
})(this);