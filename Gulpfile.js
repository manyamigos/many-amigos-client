/*****
 * Dependencies
 *****************/

var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    cssmin     = require('gulp-cssmin'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    imagemin   = require('gulp-imagemin'),
    livereload = require('gulp-livereload');

/*****
 * Configuration
 *****************/

var bases = {
    assets: 'public/assets/',

    // Usage: bases.dist('js'); // public/js
    dist: function(dir) {
        return (typeof dir === 'undefined')
            ? 'public/'
            : 'public/' + dir;
    }
};

var paths =  {
    sass      : bases.assets + 'sass/*.scss',
    images    : bases.assets + 'images/*.{png,gif,jpg}',
    scripts   : bases.assets + 'js/**/*.js',
    views     : 'app/views/**/*.php',
    templates : bases.assets + 'templates/**/*.tpl.html',
    vendor    : [
        bases.assets + 'bower_components/modernizr/modernizr.js',
        bases.assets + 'bower_components/jquery/dist/jquery.js',
        bases.assets + 'bower_components/fastclick/lib/fastclick.js',
        bases.assets + 'bower_components/foundation/js/foundation.js',
        bases.assets + 'bower_components/lodash/dist/lodash.js',
        bases.assets + 'bower_components/angular/angular.js',
        bases.assets + 'bower_components/angular-sanitize/angular-sanitize.js',
        bases.assets + 'bower_components/WOW/dist/wow.js'
    ],

    base: function(dir) {
        return { base: bases.assets + dir };
    }
};

/*****
 * Tasks
 *****************/

gulp

    .task('sass', function() {
        gulp.src(paths.sass)
            .pipe(sass())
            .pipe(gulp.dest(bases.dist('css')));

        gulp.src(paths.sass)
            .pipe(sass())
            .pipe(cssmin())
            .pipe(concat('styles.min.css'))
            .pipe(gulp.dest(bases.dist('css')))
            .pipe(livereload());
    })

    .task('components', function() {
        gulp.src(paths.vendor, paths.base('bower_components'))
            .pipe(uglify())
            .pipe(concat('vendor.min.js'))
            .pipe(gulp.dest(bases.dist('js')));
    })

    .task('scripts', function() {
        gulp.src(paths.scripts)
            .pipe(uglify())
            .pipe(concat('scripts.min.js'))
            .pipe(gulp.dest(bases.dist('js')))
            .pipe(livereload());
    })

    .task('images', function() {
        gulp.src(paths.images)
            .pipe(imagemin())
            .pipe(gulp.dest(bases.dist('img')));
    })

    .task('templates', function() {
        gulp.src(paths.templates)
            .pipe(gulp.dest(bases.dist('templates')));
    })

    .task('watch', function() {
        var server = livereload();

        gulp.watch("public/assets/sass/**/*.scss", ['sass']);
        gulp.watch(paths.scripts, ['scripts']);
        gulp.watch(paths.images, ['images']);
        gulp.watch(paths.templates, ['templates']);
        gulp.watch(paths.views).on('change', function(file) {
            server.changed(file.path);
        });
    });

/*****
 * Main task runners
 ************************/

gulp

    .task('dev', [
        'sass',
        'components',
        'scripts',
        'templates',
        'watch'
    ])

    .task('build', [
        'sass',
        'components',
        'scripts',
        'templates',
        'images'
    ]);