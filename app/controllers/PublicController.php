<?php

class PublicController extends BaseController
{

    /**
     * Home Page
     *
     * @return mixed
     */
    public function showHomePage()
    {
        $viewItems = $this->getAllMetaData('home');
        $viewItems['hideMenu'] = true;

        return View::make('public.home', $viewItems);
    }

    /**
     * How it Works Page
     *
     * @return mixed
     */
    public function showHowItWorksPage()
    {
        $viewItems = $this->getAllMetaData('how-it-works');
        $viewItems['pageTitle'] = Lang::get('how-it-works.page-title');

        return View::make('public.how-it-works', $viewItems);
    }

    /**
     * How it Works: Raising Money
     *
     * @return mixed
     */
    public function showRaisingMoneyPage()
    {
        $viewItems = $this->getAllMetaData('raising-money');
        $viewItems['pageTitle'] = Lang::get('raising-money.page-title');

        return View::make('public.raising-money', $viewItems);
    }

    /**
     * How it Works: Investing
     *
     * @return mixed
     */
    public function showInvestingPage()
    {
        $viewItems = $this->getAllMetaData('investing');
        $viewItems['pageTitle'] = Lang::get('investing.page-title');

        return View::make('public.investing', $viewItems);
    }

    /**
     * Privacy Policy
     *
     * @return mixed
     */
    public function showPrivacyPolicyPage()
    {
        $viewItems = $this->getAllMetaData('privacy-policy');
        $viewItems['pageTitle'] = Lang::get('privacy-policy.page-title');

        return View::make('public.privacy-policy', $viewItems);
    }

    /**
     * FAQ Page
     *
     * @return mixed
     */
    public function showFaqPage()
    {
        $viewItems = $this->getAllMetaData('faq');
        $viewItems['pageTitle'] = Lang::get('faq.page-title');

        return View::make('public.faq', $viewItems);
    }

    /**
     * Terms of Use Page
     * @return mixed
     */
    public function showTermsOfUsePage()
    {
        $viewItems = $this->getAllMetaData('terms-of-use');
        $viewItems['pageTitle'] = Lang::get('terms-of-use.page-title');

        return View::make('public.terms-of-use', $viewItems);
    }

    /**
     * Contact Page
     *
     * @return mixed
     */
    public function showContactPage()
    {
        $viewItems = $this->getAllMetaData('contact-us');
        $viewItems['pageTitle'] = Lang::get('contact-us.page-title');

        return View::make('public.contact', $viewItems);
    }

    /**
     * Creates an array with the available meta data for a view
     *
     * @param $view
     * @return array
     */
    private function getAllMetaData($view)
    {
        return array(

            'metaTitle'     => (Lang::has("{$view}.meta-title"))
                ? Lang::get("{$view}.meta-title")
                : null,

            'metaDescription' => (Lang::has("{$view}.meta-description"))
                ? Lang::get("{$view}.meta-description")
                : null,

            'metaKeywords'  => (Lang::has("{$view}.meta-keywords"))
                ? Lang::get(Lang::get("{$view}.meta-keywords"))
                : null,

            'ogTitle'       => (Lang::has("{$view}.og-title"))
                ? Lang::get("{$view}.og-title")
                : null,

            'ogDescription' => (Lang::has("{$view}.og-description"))
                ? Lang::get("{$view}.og-description")
                : null,

            'ogImage'       => (Lang::has("{$view}.og-image"))
                ? Lang::get("{$view}.og-image")
                : null,

        );
    }

}
