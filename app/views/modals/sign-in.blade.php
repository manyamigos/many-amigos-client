<div id="sign-in-modal" class="reveal-modal small" data-reveal data-ng-controller="SignInController">
    <h2>Sign In</h2>
    <form name="sign-in-form" data-ng-submit="signIn($event)">
        <hr>
        <br>
        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-3 columns">
                        <label for="username" class="right inline">Username</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="username" placeholder="Enter your Username" data-ng-model="credential.username" />
                    </div>
                </div>
            </div>
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-3 columns">
                        <label for="password" class="right inline">Password</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="password" id="password" placeholder="Enter your Password" data-ng-model="credential.password" />
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="small-12 columns">
            <button type="submit" class="button radius right">Sign in</button>
        </div>
    </form>
    <a class="close-reveal-modal">&#215;</a>
</div>