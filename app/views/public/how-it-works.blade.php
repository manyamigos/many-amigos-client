@extends('layouts.public')

@section('content')

<div class="row">
    <div class="large-12 columns">
        {{ Lang::get('how-it-works.body') }}
    </div>
</div>

@stop