@extends('layouts.public')

@section('content')

<div class="row">
    <div class="large-12 columns">
        {{ Lang::get('privacy-policy.body') }}
    </div>
</div>

@stop