@extends('layouts.public')

@section('content')

<div class="row">
    <div class="large-12 columns">
        {{ Lang::get('terms-of-use.body') }}
    </div>
</div>

@stop