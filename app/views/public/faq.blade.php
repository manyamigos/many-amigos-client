@extends('layouts.public')

@section('content')

<div class="row">
    <div class="large-12 columns">
        {{ Lang::get('faq.body') }}
    </div>
</div>

@stop