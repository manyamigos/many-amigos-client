@extends('layouts.public')

@section('content')

    <div class="row">
        <section class="small-12 columns home-top-bar">
            <h2 class="text-center subtitle animated fadeInDownBig">
                {{ Lang::get('home.tagline') }}
            </h2>
        </section>
    </div>

    <div class="row">
        <article class="home-call-to-action clearfix">
            <section class="medium-5 large-5 columns text-center">
                <h1 class="home-logo-wrapper">
                    <span class="far-title">
                        {{ Lang::get('home.heading') }}
                    </span>
                </h1>
                <div class="home-buttons">
                    <!-- <a href="{{ url('sign-up') }}" class="button expand radius">Pre-register</a> -->
                    <a href="http://goo.gl/forms/hXqByN6XEJ" class="button expand radius">Pre-register</a>
                    <br>
                    <span class="text-light">or</span>
                    <!-- <a href="#" data-reveal-id="sign-in-modal">Who would you give support to?</a> -->
                    <a href="http://goo.gl/forms/ynDUrR37RQ">Who would you support?</a>
                </div>
            </section>
            <section class="medium-7 large-7 columns">
                <div data-ma-video data-url="{{ Lang::get('home.video') }}"></div>
            </section>
        </article>
    </div>

    <div class="row home-separator text-center">
        <hr>
        <p class="text-light"><a href="{{ url('how-it-works') }}">Learn more <i class="fi-link"></i></a></p>
    </div>

    <div class="row">

        <article class="home-summary clearfix">
            <section class="medium-7 columns wow slideInLeft">
                <h3>{{ Lang::get('home.raise-money-heading') }}</h3>
                {{ Lang::get('home.raise-money-copy') }}
                <p class="text-right">
                    <a href="{{ url('how-it-works') }}" class="button small secondary radius">Learn more</a>
                </p>
            </section>
            <section class="medium-5 columns text-center hide-for-small wow slideInRight">
                @if (Lang::has('home.raise-money-image'))
                    {{ HTML::image(url('img/' . Lang::get('home.raise-money-image')), 'How to raise money', array('class' => 'image-circle')) }}
                @endif
            </section>
        </article>

        <article class="home-summary clearfix">
            <section class="medium-5 columns text-center hide-for-small wow slideInLeft">
                @if (Lang::has('home.investing-image'))
                    {{ HTML::image(url('img/' . Lang::get('home.investing-image')), 'How to invest money', array('class' => 'image-circle')) }}
                @endif
            </section>
            <section class="medium-7 columns wow slideInRight">
                <h3>{{ Lang::get('home.investing-heading') }}</h3>
                {{ Lang::get('home.investing-copy') }}
                <p class="text-right">
                    <a href="{{ url('how-it-works') }}" class="button small secondary radius">Learn more</a>
                </p>
            </section>
        </article>

    </div>

    <div class="row home-separator text-center">
    </div>

@stop