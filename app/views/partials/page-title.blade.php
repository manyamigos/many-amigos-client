<div class="page-title @if (isset($pageTitleClass)){{ $pageTitleClass }}@endif">
    <div class="row">
        <div class="large-12 columns">
            <h2>{{ $pageTitle }}</h2>
        </div>
    </div>
</div>